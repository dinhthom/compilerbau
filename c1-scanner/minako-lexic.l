%option noinput nounput noyywrap yylineno


%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "minako.h"
	int fileno(FILE *stream);
%}

DIGIT [0-9]
INTEGER {DIGIT}+
FLOAT {INTEGER}"."{INTEGER}|"."{INTEGER}
LETTER [a-zA-Z]
STRING {LETTER}+
%x COMMENT


%%

[ \t\n\r]   
"/*"                { BEGIN(COMMENT); }
<COMMENT>"*/"		{ BEGIN(INITIAL); }
<COMMENT>.|\n     

"//".*              { /*return ;*/ }

"=="				{ return EQ; }
"!="				{ return NEQ; }
"<"					{ return LSS; }
">"					{ return GRT; }
"<="				{ return LEQ; }
">="				{ return GEQ; }
"&&"				{ return AND; }
"||"				{ return OR; }

"bool"              { return KW_BOOLEAN; }
"do"                { return KW_DO; }
"else"              { return KW_ELSE; }
"float"             { return KW_FLOAT; }
"for"               { return KW_FOR; }
"if"                { return KW_IF; }
"int"               { return KW_INT; }
"printf"            { return KW_PRINTF; }
"return"            { return KW_RETURN; }
"void"              { return KW_VOID; }
"while"             { return KW_WHILE; }

"+"					{ return '+'; }
"-"					{ return '-'; }
"*"					{ return '*'; }
"/"					{ return '/'; }
"="					{ return '='; }

","					{ return ','; }
";"					{ return ';'; }
[(]					{ return '('; }
[)]					{ return ')'; }
[{]					{ return '{'; }
[}]					{ return '}'; }

{INTEGER}	{ 
	yylval.intValue = atoi(yytext); 
	return CONST_INT; 
}

{FLOAT}([eE]([+-])?{INTEGER})?|{INTEGER}[eE]([+-])?{INTEGER}    {
	yylval.floatValue = atof(yytext);
    return CONST_FLOAT;
}

"true"              { yylval.intValue = 1; return CONST_BOOLEAN; }
"false"             { yylval.intValue = 0; return CONST_BOOLEAN; }

"\""[^\n\"]*"\""    {
    if (yyleng == 2) {
        yylval.string = NULL;
    } else {
        yylval.string = yytext;
        for (int j = 1; j <= yyleng-2; ++j)
            yylval.string[j-1] = yylval.string[j];
        yylval.string[yyleng-2] = '\0';
    }
    return CONST_STRING;
}

({LETTER})+({DIGIT}|{LETTER})*  {
    yylval.string = yytext; 
    return ID;
}

<<EOF>>				{ return EOF; }
.					{ fprintf(stderr, "ERROR: invalid character\n"); }

%%
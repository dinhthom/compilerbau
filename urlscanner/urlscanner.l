%option noinput nounput noyywrap yylineno never-interactive

%x URL
%x LINKTEXT
%x OTHER

%{
	#include <stdio.h>
	#include <stdlib.h>
	#include "urlscanner.h"
%}

%%

<INITIAL>\<a[a-zA-Z0-9\=\"-; ]*href="\""       { BEGIN(URL); }

<URL>[^\"\>]* { 
	yylval = yytext; 
	return TOKEN_URL; 
}

<URL>\"								{ BEGIN(OTHER); }
<OTHER>\>							{ BEGIN(LINKTEXT); }
<OTHER>.|\n							{ /*return ;*/ }


<LINKTEXT>[^<]*						{ 
	                                    yylval = yytext; 
	                                    return TOKEN_TEXT; 
                                    }                                   
<LINKTEXT>\<				        { BEGIN(INITIAL); }


<INITIAL>.|\n                         { /*return ;*/ }

<<EOF>>                               { return EOF; }
%%
